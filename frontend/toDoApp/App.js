import React from 'react';
import { StyleSheet, View } from 'react-native';
import Home  from './src/screens/Home'

function App() {
  return (
    <View style={styles.container}>
      <Home />
    </View>
  );
}

const styles = StyleSheet.create({
  container:  {
    flex: 2/3,
    justifyContent: 'center',
    marginRight: 30,
    marginLeft: 30,
  }

})

export default App;