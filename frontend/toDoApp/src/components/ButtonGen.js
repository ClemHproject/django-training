import React from 'react'
import { Button, StyleSheet, View } from 'react-native'

function ButtonGen(props) {
    return (
        <View style={styles.container}>
            <Button style={styles.button} {...props} title={props.title}/>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        
    },
    button: {
        borderRadius: 10,
    }
});

export default ButtonGen;
