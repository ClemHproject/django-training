import React, { useEffect, useState } from 'react'
import ButtonGen from '../components/ButtonGen'
import { StyleSheet, View, TextInput } from 'react-native'
import axios from 'axios'
//Simple comment to test the sourceTree parameter
const initialState = {
    todoList: [],
    activableItem:{id: null, title: '', completed: false},
    editing: false,
}

const fetchTasks = async () => {
    console.log("Fetching...")

    const response = await axios.get('http://192.168.1.86:8000/api/task-list/')
    console.log(JSON.stringify(response.data))
    
}

function Home() {
    const [state, setState] = useState(initialState)
    const [value, onChangeText] = React.useState('Useless Placeholder')
    useEffect(() => {
        fetchTasks()
    }, [])

    
    return (
        <View>
            <TextInput
            style={styles.textPrez}
            onChangeText={text => onChangeText(text)}
            value={value}
    />
            <ButtonGen title="Press me" />
        </View>
    )
}

const styles = StyleSheet.create({
    textPrez: {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1,
        marginBottom: 10,
    }
})

export default Home;
